CREATE TABLE users
(
    id        SERIAL PRIMARY KEY,
    name      VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email     VARCHAR(255) NOT NULL,
    password  varchar(255) NOT NULL
);
