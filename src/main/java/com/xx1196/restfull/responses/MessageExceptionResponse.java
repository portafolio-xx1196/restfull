package com.xx1196.restfull.responses;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageExceptionResponse {

    private String message;
}
