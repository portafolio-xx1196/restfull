package com.xx1196.restfull.service;

import com.xx1196.restfull.entitys.UserEntity;
import com.xx1196.restfull.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserEntity> index() {
        return (List<UserEntity>) userRepository.findAll();
    }

    @Override
    public UserEntity store(UserEntity userEntity) {
        return userRepository.save(userEntity);
    }

    @Override
    public UserEntity update(Long id, UserEntity userEntity) {
        UserEntity userDB = userRepository.findById(id).get();

        if (Objects.nonNull(userEntity.getName()) && !"".equalsIgnoreCase(userEntity.getName())) {
            userDB.setName(userEntity.getName());
        }

        if (Objects.nonNull(userEntity.getLastName()) && !"".equalsIgnoreCase(userEntity.getLastName())) {
            userDB.setLastName(userEntity.getLastName());
        }

        if (Objects.nonNull(userEntity.getEmail()) && !"".equalsIgnoreCase(userEntity.getEmail())) {
            userDB.setEmail(userEntity.getEmail());
        }

        if (Objects.nonNull(userEntity.getPassword()) && !"".equalsIgnoreCase(userEntity.getPassword())) {
            userDB.setPassword(userEntity.getPassword());
        }

        return userRepository.save(userDB);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
