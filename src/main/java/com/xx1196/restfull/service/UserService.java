package com.xx1196.restfull.service;

import com.xx1196.restfull.entitys.UserEntity;
import java.util.List;

public interface UserService {
    List<UserEntity> index();

    UserEntity store(UserEntity userEntity);

    UserEntity update(Long id, UserEntity userEntity);

    void delete(Long id);
}
