package com.xx1196.restfull.controller;

import com.xx1196.restfull.entitys.UserEntity;
import com.xx1196.restfull.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public List<UserEntity> index() {
        return userService.index();
    }

    @PostMapping("/users")
    public UserEntity store(@Valid @RequestBody UserEntity userEntity) {
        return userService.store(userEntity);
    }

    @PutMapping("/users/{id}")
    public UserEntity update(@PathVariable("id") Long id, @RequestBody UserEntity userEntity) {
        return userService.update(id, userEntity);
    }

    @DeleteMapping("/users/{id}")
    public String delete(@PathVariable("id") Long id) {
        userService.delete(id);
        return "Delete user ID:" + id;
    }
}
